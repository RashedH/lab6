#!/bin/bash
echo -e "Zip Code Lookup"
if [ $# -eq 0 ] ; then
    echo "Usage: $0 ZIPCODE"
    exit 1
fi
city=`curl -s -dump "http://www.city-data.com/zips/$1.html" | grep '<title>' | cut -d\( -f2 | cut -d\) -f1`

echo -n "Zip Code $1 is in $city"