#!/bin/bash
echo -e "Area Code Lookup"
if [ $# -eq 0 ] ; then
    echo "Usage: $0 AREACODE"
    exit 1
fi
result="$(curl -s -dump http://www.bennetyee.org/ucsd-pages/area.html | grep "name=\"$1" | sed 's/<[^>]*>//g;s/^ //g' | cut -f2- -d\ | cut -f1 -d\( )"
echo -n "Area Code $1 is $result"